from django import forms
from .models import alamat_pengiriman, ongkos_kirim

class alamat_pengiriman_form(forms.ModelForm):
    nama_penerima = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))
    nomor_hp = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))
    provinsi = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))
    kota = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))
    kecamatan = forms.CharField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))
    kode_pos = forms.IntegerField(widget=forms.TextInput(
        attrs={
            'class' : 'form-control',
            'style' : 'border-color: #2F1487; background-color: #C4C4C4;'
        }
    ))


    nama_penerima.label = 'Nama Penerima'
    nomor_hp.label = 'Nomor Telepon'
    provinsi.label = 'Provinsi'
    kota.label = 'Kota / Kabupaten'
    kecamatan.label = 'Kecamatan'
    kode_pos.label = 'Kode Pos'

    nama_penerima.required = True
    nomor_hp.required = True
    provinsi.required = True
    kota.required = True
    kecamatan.required = True
    kode_pos.required = True

    
    class Meta:
        model = alamat_pengiriman
        fields = [
            'nama_penerima',
            'nomor_hp',
            'provinsi',
            'kota',
            'kecamatan',
            'kode_pos',
        ]

class ongkos_kirim_form(forms.ModelForm):
    biaya = forms.IntegerField()
    total = forms.IntegerField()
    biaya.required = False
    total.required = False
    class Meta:
        model = ongkos_kirim

        fields  = [
            'biaya',
            'total',
        ]

