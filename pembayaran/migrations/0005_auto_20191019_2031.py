# Generated by Django 2.2.6 on 2019-10-19 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pembayaran', '0004_auto_20191019_0241'),
    ]

    operations = [
        migrations.AddField(
            model_name='ongkos_kirim',
            name='total',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='ongkos_kirim',
            name='biaya',
            field=models.IntegerField(default=38000),
        ),
    ]
