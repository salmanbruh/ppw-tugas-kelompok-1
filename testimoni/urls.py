from django.urls import path
from testimoni.views import list_testimoni,form_testimoni
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('testimoni/', list_testimoni, name='list_testimoni'),
    path('testimoni_form/', form_testimoni, name='form_testimoni')
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)