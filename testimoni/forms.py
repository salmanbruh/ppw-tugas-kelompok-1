from django import forms
from testimoni.models import Testimoni

class TestimoniForm(forms.ModelForm):
    class Meta:
        model = Testimoni

        fields = [
            'nama',
            'testimoni',
            'image'
        ]

        labels = {
            'nama':'Nama',
            'testimoni':'Testimoni',
            'image':'Foto anda',
        }

        widgets = {
            'nama':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Masukkan nama anda',
                }
            ),
            'testimoni':forms.Textarea(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Testimoni anda',
                }
            ),
            'image':forms.ClearableFileInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'your_picture.png',
                }
            )
        }

        