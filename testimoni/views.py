from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from testimoni.models import Testimoni
from testimoni.forms import TestimoniForm
from django.conf import settings

# Create your views here.
def list_testimoni(request):
    testies = Testimoni.objects.all()
    context = {
        'testi':testies,
        'header':'Pendapat pengguna situs kami ...'
    }

    return render(request, 'testimoni/list-testimoni.html', context)

def form_testimoni(request):
    if request.method == "POST":
        form = TestimoniForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/testimoni/')
    else:
        form = TestimoniForm()

    context = {
        'page_title':'Form Testimoni',
        'form':form,
        'header':'Pendapat anda ...'
    }
    return render(request, "testimoni/form-testimoni.html", context)
