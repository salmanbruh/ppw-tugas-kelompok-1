from django.test import TestCase, Client
from django.urls import resolve
from faq.views import faq_list
from faq.models import Faq
from faq.forms import FaqForms
from django.http import HttpRequest

class FaqUnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Faq.objects.create(pertanyaan="Bayar dimann?", jawaban="Di Alfamart")

    #Test urls dan Views
    def test_faq_list_url_is_exist(self):
        response = self.client.get('/faqs/')
        self.assertEqual(response.status_code, 200)
    
    def test_new_faq_url_is_exist(self):
        response = self.client.get('/faqs')
        self.assertRedirects(response, '/faqs/', status_code=301, target_status_code=200)

    def test_faq_using_faq_list_func(self):
        found = resolve('/faqs/')
        self.assertEqual(found.func, faq_list)

    #Test Models
    def test_if_models_in_database(self):
        faqData = Faq.objects.create(pertanyaan="Bayar dimana ya?", jawaban="Di Alfamart sabi kok")
        count_faqData = Faq.objects.all().count()
        self.assertEqual(count_faqData, 2)

    def test_if_faq_pertanyaan_already_exist(self):
        pertanyaanData = Faq.objects.get(id=1)
        pertanyaan = pertanyaanData._meta.get_field('pertanyaan').verbose_name
        self.assertEqual(pertanyaan, 'pertanyaan')

    def test_if_faq_jawaban_already_exist(self):
        jawabanData = Faq.objects.get(id=1)
        jawaban = jawabanData._meta.get_field('jawaban').verbose_name
        self.assertEqual(jawaban, 'jawaban')

    #Test form
    def test_new_faq_html(self):
        form = FaqForms()
        self.assertIn('id="id_pertanyaan', form.as_p())
        self.assertIn('id="id_jawaban', form.as_p())

    def test_new_faq_validation_blank(self):
        form = FaqForms(data={'pertanyaan':'', 'jawaban':''})
        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors['pertanyaan'], ['This field is required.'])
        self.assertEquals(form.errors['jawaban'], ['This field is required.'])

    #Test HTML
    def test_faq_list_page_using_faq_list_template(self):
        response = self.client.get('/faqs/')
        self.assertTemplateUsed(response, 'faq_list.html')