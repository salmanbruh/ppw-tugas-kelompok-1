from django import forms
from .models import Faq

class FaqForms(forms.ModelForm):
    class Meta:
        model = Faq
        fields = ['pertanyaan', 'jawaban']

        label = {
            'pertanyaan':'Pertanyaan ',
            'jawaban':'Jawaban ',
        }   

        widgets = {
            'pertanyaan':forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan pertanyaan Anda',
                }
            ),
            'jawaban':forms.Textarea(
                attrs = {
                    'class' : 'form-control',
                    'placeholder':'Masukkan jawaban Anda',
                }
            )
        }
    