from django.db import models
from django.conf import settings
from django.utils import timezone

class Faq(models.Model):
    pertanyaan = models.CharField(max_length=100)
    jawaban = models.TextField(max_length=250)
    
    def __str__(self):
        return self.pertanyaan
    
    def getJawaban(self):
        return self.jawaban