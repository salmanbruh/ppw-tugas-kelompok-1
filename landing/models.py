from django.db import models

class Product(models.Model):
    name = models.TextField()
    imgProduct = models.ImageField(upload_to = "images/", default="static/default/default.jpg")
    price = models.IntegerField()
    category = models.TextField()
    description = models.TextField()